```js

obj = {
    _secret:'',
    set value(val){
        this._secret = val.split('').reverse().join('')
    },
    get value(){
        return this._secret.split('').reverse().join('')
    }
}

```