inc = (payload=1) => ({type:'INC', payload});
dec = (payload=1) => ({type:'DEC', payload});
addTodo = (payload='kup mleko') => ({type:'ADDTODO', payload});

reducer = (state,action) => {

    switch(action.type){
        case 'INC': return { 
            ...state, counter: state.counter+action.payload }
        case 'DEC': return { 
            ...state, counter: state.counter-action.payload }
        case 'ADDTODO': return { 
            ...state, todos: [...state.todos, action.payload] }
        default: return { ...state }
    }
}

state =  {
  counter: 0,
  todos:[]
}
state = reducer(state, inc())
state = reducer(state, inc(2))
state = reducer(state, addTodo('naucz sie vuex!'))
state = reducer(state, addTodo('kup mleko'))
state = reducer(state, dec())

// {counter: 2, todos: Array(2)}