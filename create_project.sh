#!/bin/sh

DIR_CHECK="/home/node/app/src/"
if [ -d "$DIR_CHECK" ]; then
  echo "Project already exists, closing"
else
  # create a sample vue project
  vue create --merge -n -p solaris-web .
  # copy the directory structure from templates repo
  yes | cp -r /mnt/* ./
  # remove unused files remaining from vue create
  rm public/favicon.ico
  rm -rf src/views
  rm src/assets/logo.png
  rm src/components/HelloWorld.vue
  # install all the necessary packages
  npm install
fi
