# Wyplenic Prosze

https://tinyurl.com/2p858ypf

https://tinyurl.com/2p 85 8y pf

# GIT

git clone https://bitbucket.org/ev45ive/solaris-sages-vuejs2.git solaris-sages-vuejs2
cd solaris-sages-vuejs2 <!-- lub File > Open Folder -> solaris-sages-vuejs2 -->
npm i
npm run serve

## Update

git stash -u
git pull -f

git pull --set-upstream origin master

# Instalacje

node -v
v16.13.1

npm -v
8.1.2

git --version
git version 2.31.1.windows.1

code -v
1.63.0

Google Chrome
96.0.4664.93

## Extensions ( Ctrl+Shift+X )

Vetur
https://marketplace.visualstudio.com/items?itemName=octref.vetur

Vue Language Features (Volar)
https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar

Prettier
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

Vue VSCode Snippets
https://marketplace.visualstudio.com/items?itemName=sdras.vue-vscode-snippets

## Emmet

https://www.jetbrains.com/help/webstorm/settings-emmet.html
https://docs.emmet.io/cheat-sheet/

## Vue Devtools

https://devtools.vuejs.org/guide/installation.html

## Vue CLI

https://cli.vuejs.org/

npm install -g @vue/cli

vue --version
@vue/cli 4.5.15

vue --help
vue create --help
vue create nazwa_katalogu

vue create -m npm --merge "."

Vue CLI v4.5.15
? Please pick a preset: Manually select features
? Check the features needed for your project:
(_) Choose Vue version
(_) Babel
( ) TypeScript

> ( ) Progressive Web App (PWA) Support
> (_) Router
> (_) Vuex
> (_) CSS Pre-processors
> (_) Linter / Formatter
> (_) Unit Testing
> (_) E2E Testing

? Choose a version of Vue.js that you want to start the project with 2.x
? Use history mode for router? (Requires proper server setup for index fallback in production) Yes
? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported by default): Sass/SCSS (with dart-sass)
? Pick a linter / formatter config: Prettier
? Pick additional lint features:
? Pick a unit testing solution: Jest
? Pick an E2E testing solution: Cypress
? Where do you prefer placing config for Babel, ESLint, etc.? In dedicated config files
? Save this as a preset for future projects? Yes
? Save preset as: solaris2

## Running Dev server

npm run serve

## CSS Selectors

https://flukeout.github.io/

## Playlists View

mkdir -p src/views/playlists/
mkdir -p src/components/playlists/

touch src/views/playlists/PlaylistsView.vue
touch src/components/playlists/PlaylistList.vue
touch src/components/playlists/PlaylistDetails.vue
touch src/components/playlists/PlaylistEditor.vue

## UI Kits

https://getbootstrap.com/docs/5.1/components/list-group/
https://bootstrap-vue.org/docs/components/list-group
https://vuetifyjs.com/en/components/lists/#action-and-item-groups
https://www.antdv.com/docs/vue/introduce/

## Storybook

https://storybook.js.org/
https://bradfrost.com/blog/post/atomic-web-design/
https://atomicdesign.bradfrost.com/chapter-2/

## Music Search View

mkdir -p src/views/search/
mkdir -p src/components/search/
mkdir -p src/components/music/

touch src/views/search/AlbumSearchView.vue
touch src/components/search/SearchForm.vue
touch src/components/search/SearchResults.vue
touch src/components/music/AlbumCard.vue
touch src/components/music/ArtistCard.vue

## Lifecycle 
https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram
https://vuejs.org/v2/api/#Options-Lifecycle-Hooks


## Logging / Sentry
https://docs.sentry.io/platforms/javascript/guides/vue/
https://ravenjs.com/ 
https://docs.sentry.io/platforms/python/

+ sourcemaps

### SVG
https://blog.logrocket.com/using-svg-and-vue-js-a-complete-guide/ 
https://vuejsexamples.com/tag/d3/ 


<template>
  <svg ref="myMap">
    <MyDevice v-for="device in devices" :pos="device.pos" @action="device.handleAction">
  </svg>
</template>

<!-- d3.select(this.$refs.myMap) -->

MyDevice:

<template>
  <g ref="myMap">
    <path d="..." @click="$emit('cosgtam') />
  </g>
</template>
