
export const counterModule = {
  namespaced: true,
  state: {
    value: 0
  },
  // 1 sync 
  mutations: {
    inc(state, payload = 1) { state.value += payload; },
    dec(state, payload = 1) { state.value -= payload; },
    reset(state, payload = 0) { state.value = payload; }
  },
  // * async
  actions: {
    startCounter({ commit, state }, payload = 0) {
      commit('reset', payload);
      const handle = setInterval(() => {
        commit('inc', 1);
        if (state.counter > 5)
          clearInterval(handle);
      }, 1000);
    }
  },
  getters: {}
};
