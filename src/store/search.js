import { fetchSearchResults } from "../services/APIService";
import { mockAlbums } from "../views/search/mockAlbums";

export const search = {
  namespaced: true,
  state: {
    query: 'batman',
    results: mockAlbums,
  },
  mutations: {
    searchStart(state, payload) { state.query = payload; },
    searchSuccess(state, payload) { state.results = payload; },
    searchFailed(state, payload) { state.results = []; },
  },
  actions: {
    async searchAlbums({ commit, state }, query = 'batman') {
      try {
        if (state.query === query) {
          commit('searchSuccess', state.results)
          return;
        }
        commit('ui/loading', true, { root: true })
        commit('ui/message', '', { root: true })
        commit('searchStart', query)
        const data = await fetchSearchResults(query)
        commit('searchSuccess', data)
      } catch (error) {
        commit('ui/message', error.message, { root: true })
        commit('searchFailed', error.message)
      }
    }
  },
  getters: {
    results: state => state.results,
    query: state => state.query,
  }
};
