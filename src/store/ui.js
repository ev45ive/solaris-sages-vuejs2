
export const ui = {
  namespaced: true,
  state: {
    loading: false,
    message: ''
  },
  mutations: {
    loading(state, loading) {
      state.loading = loading
    },
    message(state, message) {
      state.message = message
    },
  },
  actions: {},
  getters: {}
};
