import Vue from "vue";

export const dataModel = {
  namespaced: true,
  state: {
    entities: {
      playlists: {
        12334: {
          data: {},
          lastModified: 123
        }
      }
    },
    lists: {
      myplaylists: [123, 234],
      topplaylists: [123, 234]
    },
    selected: null,
  },
  mutations: {
    select(state, id) {
      state.selected = state.entities.playlists[id].data
    },
    load(state, payload) {
      state.lists.myplaylists = []
      payload.forEach(p => {
        Vue.set(state.entities.playlists[draft.id].data, draft)
        state.lists.myplaylists.push(p)
      })
    },
    updated(state, draft) {
      // state.entities.playlists[draft.id] = draft // WRONG!!
      Vue.set(state.entities.playlists[draft.id].data, draft)
    },
  },
  actions: {
    loadPlaylists() {
      // commit('ui/loading')
      // commit('ui/messsage')
      // commit('ui/messsage')
    },
    loadPlaylistById(id) { }
  },
  getters: {
    playlists(state) {
      return state.lists.myplaylists.map(id => state.entities.playlists[id].data)
    }
  }
};
