import Vue from "vue";
import Vuex from "vuex";
import { counterModule } from "./counterModule";
import { dataModel } from "./dataModel";
import { playlists } from "./playlists";
import { search } from "./search";
import { ui } from "./ui";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    counterState: counterModule,
    dataModel: dataModel,
    playlists:playlists,
    search:search,
    ui:ui,
  },
  getters: {
    counter(state) { return state.counterState.value }
  }
});
