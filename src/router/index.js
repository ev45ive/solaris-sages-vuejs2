import Vue from "vue";
import VueRouter from "vue-router";
import PlaylistsView from "../views/playlists/PlaylistsView.vue";
import AlbumSearchView from "../views/search/AlbumSearchView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: '/search'
  },
  {
    path: "/playlists/:playlistId?",
    name: "playlists",
    component: PlaylistsView,
    // children: [
    //   {
    //     path: '', // /playlists/123/,
    //     name: 'details',
    //     component: PlaylistDetails
    //   },
    //   {
    //     path: '/form' // /playlists/123/form
    //   },
    // ]
  },
  {
    path: "/search",
    name: "search",
    component: AlbumSearchView,
  },
  // {
  //   path: "/access_token*",
  //   redirect: '/search'
  // },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  // mode: "hash",
  base: process.env.BASE_URL,
  routes,
  linkActiveClass: 'active',
  // linkExactActiveClass:'exact-active',
});

export default router;

router.beforeEach((to, from, next) => {
  // ...
  next()
})

// router.onError((error) => {
//   console.error(error)
// })