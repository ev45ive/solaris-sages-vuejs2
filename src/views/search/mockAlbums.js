
// fakerjs / casualjs
export const mockAlbums = [
  {
    id: "123",
    images: [{ url: "https://www.placecage.com/c/300/300" }],
    name: "Album 123",
  },
  {
    id: "234",
    images: [{ url: "https://www.placecage.com/c/200/200" }],
    name: "Album 234",
  },
  {
    id: "345",
    images: [{ url: "https://www.placecage.com/c/400/400" }],
    name: "Album 345",
  },
  {
    id: "456",
    images: [{ url: "https://www.placecage.com/c/500/500" }],
    name: "Album 456",
  },
];
