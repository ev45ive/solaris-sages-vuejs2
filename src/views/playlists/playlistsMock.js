export const playlistsMock = [
  {
    id: "123",
    name: "Playlist 123",
    public: true,
    description: "Moja playlista",
  },
  {
    id: "234",
    name: "Playlist 234",
    public: false,
    description: "Moja playlista",
  },
  {
    id: "345",
    name: "Playlist 345",
    public: true,
    description: "Moja playlista",
  },
];
