import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Button from "./components/Button.vue";
import * as auth from "./services/AuthService";

Vue.config.productionTip = false;

auth.init()

Vue.component('Button', Button)

Vue.filter('titlecase', (value) => {
  return value.substr(0, 1).toUpperCase() + value.substr(1);
})

window.app = new Vue({
  router,
  store,
  render: (h) => h(App),
})
  .$mount("#app");