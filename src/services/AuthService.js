import axios from "axios";

let token = ''

// holoyis165@bulkbye.com
// placki777

export const login = () => {
  var client_id = 'bbf274fee0ef483b9a909649797c6fc8';
  var redirect_uri = window.location.origin + '/';

  // var state = generateRandomString(16);
  // localStorage.setItem(stateKey, state);

  var scope = 'user-read-private user-read-email';

  var url = 'https://accounts.spotify.com/authorize';
  url += '?response_type=token';
  url += '&client_id=' + encodeURIComponent(client_id);
  url += '&scope=' + encodeURIComponent(scope);
  url += '&redirect_uri=' + encodeURIComponent(redirect_uri);
  // url += '&state=' + encodeURIComponent(state);

  window.location.href = url;
}

export const logout = () => {
  sessionStorage.removeItem('token')
  login()
}
export const getToken = () => {
  return token;
}

export const init = () => {
  if (window.location.hash) {
    token = new URLSearchParams(window.location.hash).get('#access_token')
  }

  if (token) {
    sessionStorage.setItem('token', token)
    window.location.hash = ''
    window.location.path = '/search'
  } else {
    token = sessionStorage.getItem('token')
  }

  if (!token) {
    login()
  }
}


axios.defaults.baseURL = 'https://api.spotify.com/v1/'
axios.interceptors.request.use(config => {
  return {
    ...config,
    headers: {
      ...config.headers,
      Authorization: 'Bearer ' + getToken()
    }
  }
})
axios.interceptors.response.use(responseWentOk => responseWentOk, (error) => {
  console.error(error)
  
  if (!axios.isAxiosError(error)) {
    throw new Error('Unexpected server error')
  }
  if (error.response.status === 401) {
    logout()
    throw new Error('Not logged in')
  }
  throw new Error(error.response.data.error.message)
})