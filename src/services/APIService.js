import axios from 'axios';

export const fetchSearchResults = async (query) => {
  const res = await axios.get('search', {
    params: {
      type: 'album', query
    },
    transformResponse: [].concat(axios.defaults.transformResponse, (data) => {
      return data.albums.items
    })
  })
  return res.data
}

export const fetchPlaylists = async () => {
  const res = await axios.get('me/playlists', {
    transformResponse: [].concat(axios.defaults.transformResponse, (data) => data.items)
  })
  return res.data
}

export const fetchPlaylistById = async (id) => {
  const res = await axios.get('playlists/' + id)
  return res.data
}
export const fetchCurrentUser = async () => {
  const res = await axios.get('me')
  return res.data
}